# Translation of plasmaintegration5.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2014, 2016, 2017.
msgid ""
msgstr ""
"Project-Id-Version: plasmaintegration5\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-01 23:30+0000\n"
"PO-Revision-Date: 2017-09-28 17:58+0200\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@ijekavian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: platformtheme/kdeplatformfiledialoghelper.cpp:283
#, kde-format
msgctxt "@title:window"
msgid "Open File"
msgstr "Отварање фајла"

#: platformtheme/kdeplatformfiledialoghelper.cpp:284
#, kde-format
msgctxt "@title:window"
msgid "Save File"
msgstr "Уписивање фајла"

#: platformtheme/kdeplatformtheme.cpp:490
#, kde-format
msgctxt "@action:button"
msgid "Save All"
msgstr "Сачувај све"

#: platformtheme/kdeplatformtheme.cpp:494
#, kde-format
msgctxt "@action:button"
msgid "&Yes"
msgstr ""

#: platformtheme/kdeplatformtheme.cpp:496
#, kde-format
msgctxt "@action:button"
msgid "Yes to All"
msgstr "Да за све"

#: platformtheme/kdeplatformtheme.cpp:498
#, kde-format
msgctxt "@action:button"
msgid "&No"
msgstr ""

#: platformtheme/kdeplatformtheme.cpp:500
#, kde-format
msgctxt "@action:button"
msgid "No to All"
msgstr "Не за све"

#: platformtheme/kdeplatformtheme.cpp:503
#, kde-format
msgctxt "@action:button"
msgid "Abort"
msgstr "Обустави"

#: platformtheme/kdeplatformtheme.cpp:505
#, kde-format
msgctxt "@action:button"
msgid "Retry"
msgstr "Покушај поново"

#: platformtheme/kdeplatformtheme.cpp:507
#, kde-format
msgctxt "@action:button"
msgid "Ignore"
msgstr "Игнориши"

#: platformtheme/kdirselectdialog.cpp:120
#, kde-format
msgctxt "folder name"
msgid "New Folder"
msgstr "Нова фасцикла"

#: platformtheme/kdirselectdialog.cpp:126
#, kde-format
msgctxt "@title:window"
msgid "New Folder"
msgstr "Нова фасцикла"

#: platformtheme/kdirselectdialog.cpp:127
#, kde-format
msgctxt "@label:textbox"
msgid ""
"Create new folder in:\n"
"%1"
msgstr ""
"Направи нову фасциклу у:\n"
"‘%1’"

#: platformtheme/kdirselectdialog.cpp:158
#, kde-format
msgid "A file or folder named %1 already exists."
msgstr "Фајл или фасцикла по имену ‘%1’ већ постоји."

#: platformtheme/kdirselectdialog.cpp:167
#, kde-format
msgid "You do not have permission to create that folder."
msgstr "Немате дозволу да направите ту фасциклу."

#: platformtheme/kdirselectdialog.cpp:269
#, kde-format
msgctxt "@title:window"
msgid "Select Folder"
msgstr "Избор фасцикле"

#: platformtheme/kdirselectdialog.cpp:278
#, kde-format
msgctxt "@action:button"
msgid "New Folder..."
msgstr "Нова фасцикла..."

#: platformtheme/kdirselectdialog.cpp:327
#, kde-format
msgctxt "@action:inmenu"
msgid "New Folder..."
msgstr "Нова фасцикла..."

#: platformtheme/kdirselectdialog.cpp:336
#, kde-format
msgctxt "@action:inmenu"
msgid "Move to Trash"
msgstr "Премјести у смеће"

#: platformtheme/kdirselectdialog.cpp:345
#, kde-format
msgctxt "@action:inmenu"
msgid "Delete"
msgstr "Обриши"

#: platformtheme/kdirselectdialog.cpp:356
#, kde-format
msgctxt "@option:check"
msgid "Show Hidden Folders"
msgstr "Скривене фасцикле"

#: platformtheme/kdirselectdialog.cpp:363
#, kde-format
msgctxt "@action:inmenu"
msgid "Properties"
msgstr "Својства"

#: platformtheme/kfiletreeview.cpp:185
#, kde-format
msgid "Show Hidden Folders"
msgstr "Скривене фасцикле"
