# Translation of plasmaintegration5.po to Catalan
# Copyright (C) 2016-2022 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2016, 2017, 2022.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma-integration\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-01 23:30+0000\n"
"PO-Revision-Date: 2022-11-04 13:25+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 20.12.0\n"

#: platformtheme/kdeplatformfiledialoghelper.cpp:283
#, kde-format
msgctxt "@title:window"
msgid "Open File"
msgstr "Obre un fitxer"

#: platformtheme/kdeplatformfiledialoghelper.cpp:284
#, kde-format
msgctxt "@title:window"
msgid "Save File"
msgstr "Desa el fitxer"

#: platformtheme/kdeplatformtheme.cpp:490
#, kde-format
msgctxt "@action:button"
msgid "Save All"
msgstr "Desa-ho tot"

#: platformtheme/kdeplatformtheme.cpp:494
#, kde-format
msgctxt "@action:button"
msgid "&Yes"
msgstr "&Sí"

#: platformtheme/kdeplatformtheme.cpp:496
#, kde-format
msgctxt "@action:button"
msgid "Yes to All"
msgstr "Sí a tot"

#: platformtheme/kdeplatformtheme.cpp:498
#, kde-format
msgctxt "@action:button"
msgid "&No"
msgstr "&No"

#: platformtheme/kdeplatformtheme.cpp:500
#, kde-format
msgctxt "@action:button"
msgid "No to All"
msgstr "No a tot"

#: platformtheme/kdeplatformtheme.cpp:503
#, kde-format
msgctxt "@action:button"
msgid "Abort"
msgstr "Interromp"

#: platformtheme/kdeplatformtheme.cpp:505
#, kde-format
msgctxt "@action:button"
msgid "Retry"
msgstr "Reintenta"

#: platformtheme/kdeplatformtheme.cpp:507
#, kde-format
msgctxt "@action:button"
msgid "Ignore"
msgstr "Ignora"

#: platformtheme/kdirselectdialog.cpp:120
#, kde-format
msgctxt "folder name"
msgid "New Folder"
msgstr "Carpeta nova"

#: platformtheme/kdirselectdialog.cpp:126
#, kde-format
msgctxt "@title:window"
msgid "New Folder"
msgstr "Carpeta nova"

#: platformtheme/kdirselectdialog.cpp:127
#, kde-format
msgctxt "@label:textbox"
msgid ""
"Create new folder in:\n"
"%1"
msgstr ""
"Crea una carpeta nova a:\n"
"%1"

#: platformtheme/kdirselectdialog.cpp:158
#, kde-format
msgid "A file or folder named %1 already exists."
msgstr "Ja existeix un fitxer o una carpeta anomenats %1."

#: platformtheme/kdirselectdialog.cpp:167
#, kde-format
msgid "You do not have permission to create that folder."
msgstr "No teniu permís per a crear aquesta carpeta."

#: platformtheme/kdirselectdialog.cpp:269
#, kde-format
msgctxt "@title:window"
msgid "Select Folder"
msgstr "Selecció d'una carpeta"

#: platformtheme/kdirselectdialog.cpp:278
#, kde-format
msgctxt "@action:button"
msgid "New Folder..."
msgstr "Carpeta nova..."

#: platformtheme/kdirselectdialog.cpp:327
#, kde-format
msgctxt "@action:inmenu"
msgid "New Folder..."
msgstr "Carpeta nova..."

#: platformtheme/kdirselectdialog.cpp:336
#, kde-format
msgctxt "@action:inmenu"
msgid "Move to Trash"
msgstr "Mou a la paperera"

#: platformtheme/kdirselectdialog.cpp:345
#, kde-format
msgctxt "@action:inmenu"
msgid "Delete"
msgstr "Suprimeix"

#: platformtheme/kdirselectdialog.cpp:356
#, kde-format
msgctxt "@option:check"
msgid "Show Hidden Folders"
msgstr "Mostra les carpetes ocultes"

#: platformtheme/kdirselectdialog.cpp:363
#, kde-format
msgctxt "@action:inmenu"
msgid "Properties"
msgstr "Propietats"

#: platformtheme/kfiletreeview.cpp:185
#, kde-format
msgid "Show Hidden Folders"
msgstr "Mostra les carpetes ocultes"
